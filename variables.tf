variable "region" {
  type = string
  description = "https://cloud.google.com/compute/docs/regions-zones#locations"
}

variable "project" {
  type = string
  description = "ID do Projeto"
}

variable "gcp_service_list" {
  description = "List of GCP service to be enabled for a project."
  type        = list
}

variable "storage_class" {
  type        = string
  description = "The storage class of the Storage Bucket to create"
}

variable "topic" { 
  type = string
  description = "Nome do topico do Pub/Sub"
}