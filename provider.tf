# Versao do terraform a ser utilizada
# https://www.terraform.io/downloads.html

terraform {
  required_version = ">= 0.12"
}


provider "google" {
  project     = "var.project"
  region      = "var.region"
}