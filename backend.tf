# Cria o bucket para armazenar o arquivo de state do terraform

resource "google_storage_bucket" "tfstate" {
  name     = "${var.project}-tfstate"
  location = var.region
  project  = var.project
  force_destroy = true
  storage_class = var.storage_class
  versioning {
    enabled = true
  }
}

#terraform {
#  backend "gcs" {
#    bucket  = "google_storage_bucket.tfstate.name"
#    prefix  = "dev/terraform.tfstate"
#  }
#}