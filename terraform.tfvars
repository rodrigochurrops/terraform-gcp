storage_class = "REGIONAL"
project = "aerobic-library-277313"
region = "us-central1"
topic = "meu_topico"

gcp_service_list = [
  "bigquery.googleapis.com",           # BigQuery API
  "cloudapis.googleapis.com",          # Google Cloud APIs
  "cloudbuild.googleapis.com",         # Cloud Build API
  "clouddebugger.googleapis.com",      # Cloud Debugger API
  "compute.googleapis.com",            # Compute Engine API
  "containerregistry.googleapis.com",  # Container Registry API
  "logging.googleapis.com",            # Cloud Logging API
  "monitoring.googleapis.com",         # Cloud Monitoring API
  "pubsub.googleapis.com",             # Cloud Pub/Sub API
  "secretmanager.googleapis.com",      # Secret Manager API
  "sourcerepo.googleapis.com",         # Cloud Source Repositories API
  "stackdriver.googleapis.com",        # Stackdriver API
  "storage-component.googleapis.com"   # Cloud Storage
]