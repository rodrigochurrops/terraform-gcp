resource "google_pubsub_topic" "pubsub-topic" {
  name    = var.topic
  project = var.project

  message_storage_policy {
    allowed_persistence_regions = [
      var.region,
    ]
  }

  labels = {
    foo = "bar"
  }
}