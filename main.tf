# Habilita as APIs para os serviços GPC utilizados
resource "google_project_service" "gcp_services" {
  count   = length(var.gcp_service_list)
  project = var.project
  service = var.gcp_service_list[count.index]

  disable_dependent_services = true
}